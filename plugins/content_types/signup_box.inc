<?php
/**
 * @file
 * Signup box content type definitions.
 */

/**
 * Plugin's definition.
 *
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('Peytz Mail signup box'),
  'description' => t('Newsletter signup box with multiple lists option.'),
  'single' => TRUE,
  'defaults' => array(),
  'category' => array(t('Peytz'), -9),
);

/**
 * Run-time rendering of the body of the block.
 */
function peytz_mail_signup_box_content_type_render($subtype, $conf, $args, $context) {
  $block = new stdClass();

  if (!empty($conf['override_title']) && !empty($conf['override_title_text'])) {
    $block->title = check_plain($conf['override_title_text']);
  }
  else {
    $block->title = t('Newsletter signup');
  }
  if (!empty($conf)) {
    $block->content = drupal_get_form('peytz_mail_signup_form', $conf);
  }

  return $block;

}

/**
 * Edit form callback for the content type.
 */
function peytz_mail_signup_box_content_type_edit_form($form, &$form_state) {
  // Add the options fields.
  peytz_mail_signup_form_option_fields($form, $form_state['conf']);

  return $form;
}

/**
 * Submit form callback for the content type.
 */
function peytz_mail_signup_box_content_type_edit_form_submit($form, &$form_state) {
  $form_state['conf'] = $form_state['values'];

  if (isset($form_state['values']['newsletter_lists'])) {
    $mailinglists = peytz_mail_get_mailing_lists();

    $selected_lists = array();
    foreach ($form_state['values']['newsletter_lists'] as $list) {
      $selected_lists[] = $mailinglists[$list];
    }

    $form_state['conf']['newsletter_lists'] = serialize($selected_lists);
  }
}

/**
 * Panels admin render panel.
 */
function peytz_mail_signup_box_content_type_admin_info($subtype, $conf, $contexts) {
  $block = new stdClass();
  $list_conf = unserialize($conf['newsletter_lists']);
  $list_titles = array();
  foreach ($list_conf as $list) {
    $list_titles[] = $list['title'];
  }
  $block->title = t(
    'Newsletter signup box<br />%lists',
    array(
      '%lists' => implode(", ", $list_titles),
    )
  );
  return $block;
}
