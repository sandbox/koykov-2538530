<?php
/**
 * @file
 * Peytz Mail module admin pages.
 */

 /**
  * Module setting form.
  */
function peytz_mail_settings_form($form, &$form_state) {
  // Test the API settings but make sure that it is only done on view and not
  // submit by checking the $form_state['values'].
  if (empty($form_state['values'])) {
    $test = peytz_mail_api_call('mailinglists', 'GET', array(), TRUE);
    if ($test['code'] == 200) {
      drupal_set_message(t('Your site is connected to the Peytz Mail API.'));
    }
    elseif ($test['code'] == 0) {
      drupal_set_message(t('Connection timed out. Please check your Service URL.'), 'error');
    }
    else {
      $response = drupal_json_decode($test['response']);
      $response_type = array_keys($response);
      drupal_set_message($response[$response_type[0]], $response_type[0]);
    }
  }

  $form['peytz_mail_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Service URL'),
    '#size' => 40,
    '#maxlength' => 255,
    '#default_value' => variable_get('peytz_mail_url', ''),
  );

  $form['peytz_mail_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('API key'),
    '#description' => t('API key for the Peytz Mail service'),
    '#size' => 40,
    '#maxlength' => 255,
    '#default_value' => variable_get('peytz_mail_api_key', ''),
  );

  // Add the options fields.
  peytz_mail_signup_form_option_fields($form);

  $form['#submit'][] = 'peytz_mail_settings_form_submit';
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );
  return $form;
}

/**
 * Submit callback for the admin settings page.
 *
 * Using a custom submit callback because of the default settings and using a
 * common function for adding the options fields to the forms.
 */
function peytz_mail_settings_form_submit($form, &$form_state) {
  variable_set('peytz_mail_url', $form_state['values']['peytz_mail_url']);
  variable_set('peytz_mail_api_key', $form_state['values']['peytz_mail_api_key']);
  variable_set('peytz_mail_default_name_field_setting', $form_state['values']['name_field_setting']);
  if (isset($form_state['values']['newsletter_lists'])) {
    variable_set('peytz_mail_default_newsletter_lists', $form_state['values']['newsletter_lists']);
  }
  if (isset($form_state['values']['hide_newsletter_lists'])) {
    variable_set('peytz_mail_default_hide_newsletter_lists', $form_state['values']['hide_newsletter_lists']);
  }
  variable_set('peytz_mail_default_skip_confirm', $form_state['values']['skip_confirm']);
  variable_set('peytz_mail_default_skip_welcome', $form_state['values']['skip_welcome']);
}
