<?php
/**
 * @file
 * Peytz Mail API module.
 */

/**
 * Implements hook_menu().
 */
function peytz_mail_menu() {
  // Administration pages.
  $items['admin/config/peytz_mail'] = array(
    'title' => 'Peytz Mail API',
    'description' => 'Configure Peytz Mail API.',
    'position' => 'right',
    'weight' => -20,
    'page callback' => 'system_admin_menu_block_page',
    'access arguments' => array('administer site configuration'),
    'file' => 'system.admin.inc',
    'file path' => drupal_get_path('module', 'system'),
  );

  $items['admin/config/peytz_mail/settings'] = array(
    'title' => 'Settings',
    'description' => 'Settings for the Peytz Mail API.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('peytz_mail_settings_form'),
    'access arguments' => array('administer peytz mail settings'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'peytz_mail.admin.inc',
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function peytz_mail_permission() {
  return array(
    'administer peytz mail settings' => array(
      'title' => t('Administer Peytz Mail'),
      'description' => t('Administer Peytz Mail module settings.'),
    ),
  );
}

/**
 * Implements hook_cron().
 *
 * @todo : describe what happens here and what the fetched data is used for. Is
 *   this basically the whole base of the newsletter lists?
 */
function peytz_mail_cron() {
  $result = db_select('peytz_mail_api_queue', 'q')
    ->fields('q')
    ->execute();
  foreach ($result as $item) {
    $return_data = peytz_mail_api_call($item->function, $item->method, unserialize($item->parameters));

    /*
     * @todo : Some of this belongs in CRUD functions.
     */

    if ($return_data['code'] != 0) {
      db_delete('peytz_mail_api_queue')
        ->condition('cid', $item->cid, '=')
        ->execute();
    }
    else {
      $data = array(
        'cid' => $item->cid,
        'attempts' => ++$item->attempts,
        'updated' => time(),
      );
      drupal_write_record('peytz_mail_api_queue', $data, array('cid'));
    }
  }
}

/**
 * Implements hook_help().
 */
function peytz_mail_help($path, $arg) {
  switch ($path) {
    case 'admin/help#peytz_mail':
      // Return a line-break version of the module README.txt
      return check_markup(file_get_contents(dirname(__FILE__) . "/README.txt"));
  }
}

/**
 * Implements hook_form().
 */
function peytz_mail_signup_form($form, &$form_state, $conf) {
  if (empty($conf['newsletter_lists'])) {
    return;
  }

  if ($conf['name_field_setting'] == 'single') {
    $form['full_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Name'),
      '#required' => TRUE,
    );
  }
  elseif ($conf['name_field_setting'] == 'double') {
    $form['first_name'] = array(
      '#type' => 'textfield',
      '#title' => t('First name'),
      '#required' => TRUE,
    );
    $form['last_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Last name'),
      '#required' => TRUE,
    );
  }

  $form['email'] = array(
    '#type' => 'textfield',
    '#title' => t('E-mail'),
    '#required' => TRUE,
  );

  /*
   * @todo : Handle this unserialize locally from caller.
   */
  $newsletter_lists = is_array($conf['newsletter_lists']) ? $conf['newsletter_lists'] : unserialize($conf['newsletter_lists']);
  $newsletter_list_options = array();
  if (!empty($newsletter_lists)) {
    foreach ($newsletter_lists as $list) {
      $newsletter_list_options[$list['id']] = $list['title'];
    }
  }

  // Get custom fields from other modules.
  foreach (module_implements('peytz_mail_subcribe_form_fields') as $module) {
    $function = $module . '_peytz_mail_subcribe_form_fields';
    $function($form, $newsletter_list_options);
  }

  if (count($newsletter_list_options) == 1 || $conf['hide_newsletter_lists']) {
    foreach ($newsletter_lists as $list) {
      $form['newsletter_list_signup_' . $list['id']] = array(
        '#type' => 'hidden',
        '#value' => 1,
      );
    }
  }
  else {
    foreach ($newsletter_lists as $list) {
      $form['newsletter_list_signup_' . $list['id']] = array(
        '#type' => 'checkbox',
        '#title' => check_plain($list['title']),
        '#description' => check_plain($list['description']),
      );
    }
  }

  $form['skip_confirm'] = array(
    '#type' => 'hidden',
    '#value' => $conf['skip_confirm'] ? TRUE : FALSE,
  );
  $form['skip_welcome'] = array(
    '#type' => 'hidden',
    '#value' => $conf['skip_welcome'] ? TRUE : FALSE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Signup'),
  );

  return $form;
}

/**
 * Peytz Mail signup form.
 */
function peytz_mail_signup_form_validate($form, &$form_state) {
  $keys = array_keys($form_state['values']);

  /*
   * @todo: Add doc block descriping the process below. It looks a bit hacked?
   * - Add ['fieldset']['#tree'] => TRUE is ya friend.
   */

  $list_fields = preg_grep('/^newsletter_list_signup_/', $keys);
  $list_fields = array_values($list_fields);
  $lists_selected = array();
  foreach ($list_fields as $field) {
    if (!empty($form_state['values'][$field])) {
      $lists_selected[] = str_replace('newsletter_list_signup_', '', $field);
    }
  }

  if (empty($lists_selected)) {
    form_set_error($list_fields[0], t('Please select which newsletter to sign up for.'));
  }
  else {
    // Do the subscribe API call in the validate funcion so we can return error
    // message to the form.
    $parameters['subscribe']['subscriber']['email'] = $form_state['values']['email'];
    if (!empty($form_state['values']['full_name'])) {
      $parameters['subscribe']['subscriber']['full_name'] = $form_state['values']['full_name'];
    }
    if (!empty($form_state['values']['first_name'])) {
      $parameters['subscribe']['subscriber']['first_name'] = $form_state['values']['first_name'];
    }
    if (!empty($form_state['values']['last_name'])) {
      $parameters['subscribe']['subscriber']['last_name'] = $form_state['values']['last_name'];
    }
    $parameters['subscribe']['mailinglist_ids'] = $lists_selected;
    $parameters['subscribe']['skip_confirm'] = $form_state['values']['skip_confirm'];
    $parameters['subscribe']['skip_welcome'] = $form_state['values']['skip_welcome'];

    // Handle custom fields.
    $custom_fields = preg_grep('/^peytz_mail_custom_field_/', $keys);
    $custom_fields = array_values($custom_fields);
    foreach ($custom_fields as $field) {
      if (!empty($form_state['values'][$field])) {
        $parameters['subscribe']['subscriber'][str_replace('peytz_mail_custom_field_', '', $field)] = $form_state['values'][$field];
      }
    }

    /*
     * @todo:
     * - Move validation/response to separate global function.
     * _validate handler validates input REGARDLESS of what happens after. All
     * - signup processing belongs in the _submit handler.
     */

    $return = peytz_mail_api_call('mailinglists/subscribe', 'POST', $parameters);
    $response = drupal_json_decode($return['response']);

    switch ($return['code']) {
      case 0:
      case 500:
        // Connection error or error in API, save to queue.
        peytz_mail_add_to_api_queue('mailinglists/subscribe', 'POST', $parameters);
        break;

      case 201:
        drupal_set_message(t('Newsletter signup successful'));
        break;

      case 400:
      case 401:
      case 404:
      case 422:
        form_set_error('email', $response['message']);
        break;
    }
  }
}

/**
 * Implements hook_block_info().
 */
function peytz_mail_block_info() {
  $blocks['peytz_mail_signup_box'] = array(
    'info' => t('Peytz Mail signup box'),
    'cache' => DRUPAL_NO_CACHE,
  );

  return $blocks;
}

/**
 * Implements hook_block_configure().
 */
function peytz_mail_block_configure($delta = '') {

  /*
   * @todo: Config hijacked from similar content pane.
   */

  $form = array();
  if ($delta == 'peytz_mail_signup_box') {
    $conf = variable_get($delta . '_block_conf', array());

    // Add the options fields.
    peytz_mail_signup_form_option_fields($form, $conf);
  }
  return $form;
}

/**
 * Implements hook_block_save().
 */
function peytz_mail_block_save($delta = '', $edit = array()) {
  if ($delta == 'peytz_mail_signup_box') {
    if (isset($edit['newsletter_lists'])) {
      $mailinglists = peytz_mail_get_mailing_lists();

      $selected_lists = array();
      foreach ($edit['newsletter_lists'] as $list) {
        $selected_lists[] = $mailinglists[$list];
      }

      $edit['newsletter_lists'] = $selected_lists;
    }
    variable_set($delta . '_block_conf', $edit);
  }
}

/**
 * Implements hook_block_view().
 */
function peytz_mail_block_view($delta = '') {
  /*
   * @todo: Hijack pane render function.
   */

  // This example comes from node.module. Note that you can also return a
  // renderable array rather than rendered HTML for 'content'.
  $block = array();

  switch ($delta) {
    case 'peytz_mail_signup_box':
      $conf = variable_get($delta . '_block_conf', array());

      if (!empty($conf['override_title']) && !empty($conf['override_title_text'])) {
        $block['subject'] = check_plain($conf['override_title_text']);
      }
      else {
        $block['subject'] = t('Newsletter signup');
      }
      if (!empty($conf)) {
        $block['content'] = drupal_get_form('peytz_mail_signup_form', $conf);
      }
      break;

  }
  return $block;
}

/**
 * Retrieve mailing lists from Peytz Mail API.
 *
 * @todo : You have two functions: fetch mailing lists and get mailing lists.
 *   - Fetch fetches from remote.
 *   - Get returns the stored mailinglists.
 * @todo : How is this implemented?
 *
 * @param string $id
 *   Applying a mailing list id will return that single mailing list.
 *
 * @return array
 *   Available mailing lists.
 */
function peytz_mail_get_mailing_lists($id = NULL) {
  if (empty($id)) {
    $mailing_list_result = peytz_mail_api_call('mailinglists', 'GET');
  }
  else {
    $mailing_list_result = peytz_mail_api_call('mailinglists/' . $id, 'GET');
  }
  if (!empty($mailing_list_result['response'])) {
    $data = drupal_json_decode($mailing_list_result['response']);
  }

  if ($mailing_list_result['code'] != 200 && $data) {
    // @todo: remove this when I'm done figuring out how to handle all errors.
    watchdog(
      'Peytz Mail', 'Return code !code. Return data !data',
      array(
        '!code' => $mailing_list_result['code'],
        '!data' => print_r($data, TRUE),
      ),
      WATCHDOG_DEBUG
    );
  }

  $list_data = array();
  if (isset($data['mailinglists'])) {
    $list_data = $data['mailinglists'];
  }
  elseif (isset($data['mailinglist'])) {
    $list_data[] = $data['mailinglist'];
  }

  $mailinglists = array();
  foreach ($list_data as $list) {
    if ($list['public'] === TRUE) {
      $mailinglists[$list['id']] = array(
        'id' => check_plain($list['id']),
        'title' => check_plain($list['title']),
        'description' => check_plain($list['description']),
        'weight' => $list['public_position'],
      );

    }
  }
  return $mailinglists;
}

/**
 * Peytz Mail API call.
 *
 * @param string $function
 *   API function name.
 * @param string $method
 *   API call method.
 * @param array $data
 *   API call parameters.
 * @param bool $silent
 *   Set TRUE to squelch error messages.
 *
 * @todo : silent needs to be TRUE by default.
 *
 * @return array
 *   API call result.
 */
function peytz_mail_api_call($function, $method, $data = array(), $silent = FALSE) {
  $json_data = drupal_json_encode($data);

  $api_key = variable_get('peytz_mail_api_key', '');
  $url = variable_get('peytz_mail_url', '');
  $headers = array('Accept: application/json', 'Content-Type: application/json');
  $path = '/api/v1/' . $function . '.json';

  $handle = curl_init();
  curl_setopt($handle, CURLOPT_URL, $url . $path);
  curl_setopt($handle, CURLOPT_HTTPHEADER, $headers);
  curl_setopt($handle, CURLOPT_RETURNTRANSFER, TRUE);
  curl_setopt($handle, CURLOPT_SSL_VERIFYHOST, FALSE);
  curl_setopt($handle, CURLOPT_USERPWD, $api_key . ':');
  curl_setopt($handle, CURLOPT_TIMEOUT_MS, 2000);

  switch ($method) {
    case 'PUT':
      curl_setopt($handle, CURLOPT_CUSTOMREQUEST, 'PUT');
      curl_setopt($handle, CURLOPT_POSTFIELDS, $json_data);
      break;

    case 'DELETE':
      curl_setopt($handle, CURLOPT_CUSTOMREQUEST, 'DELETE');
      curl_setopt($handle, CURLOPT_POSTFIELDS, $json_data);
      break;

    case 'POST':
      curl_setopt($handle, CURLOPT_POST, TRUE);
      curl_setopt($handle, CURLOPT_POSTFIELDS, $json_data);
      break;

    // GET is default.
    default:
      curl_setopt($handle, CURLOPT_HTTPGET, TRUE);
      break;
  }

  $response = curl_exec($handle);
  $code = curl_getinfo($handle, CURLINFO_HTTP_CODE);

  if ($silent === FALSE) {
    switch ($code) {
      case 401:
        $arr_response = drupal_json_decode($response);
        drupal_set_message(t('%api_message', array('%api_message' => $arr_response['error'])), 'error');
        watchdog('peytz_mail', '!api_message', array('!api_message' => $arr_response['error']), WATCHDOG_ERROR);
        break;

      case 404:
        drupal_set_message(t('API call not found.'), 'error');
        watchdog('peytz_mail', 'API call !function not found', array('!function' => $function), WATCHDOG_ERROR);
        break;
    }
  }

  return array('code' => $code, 'response' => $response);
}

/**
 * Save API call to queue.
 *
 * @param string $function
 *   API function name.
 * @param string $method
 *   API call method.
 * @param array $parameters
 *   API call parameters.
 */
function peytz_mail_add_to_api_queue($function, $method, $parameters = array()) {
  $data = array(
    'function' => $function,
    'method' => $method,
    'parameters' => serialize($parameters),
    'attempts' => 1,
    'updated' => time(),
    'created' => time(),
  );
  drupal_write_record('peytz_mail_api_queue', $data);
}

/**
 * Implements hook_ctools_plugin_directory().
 */
function peytz_mail_ctools_plugin_directory($module, $plugin) {
  if ($module == 'ctools' && $plugin == 'content_types') {
    return "plugins/$plugin";
  }
}

/**
 * Common form fields for newsletter form options.
 *
 * @param array $form
 *   The form array being altered.
 * @param array $values
 *   Default field values.
 */
function peytz_mail_signup_form_option_fields(&$form = array(), $values = array()) {
  // Name field settings.
  $default_name_setting = variable_get('peytz_mail_default_name_field_setting', 0);
  $form['name_field_setting'] = array(
    '#type' => 'select',
    '#title' => t('Name field setup'),
    '#multiple' => FALSE,
    '#description' => t('Select to display the name field as a single full name field, as first name and last name fields, or turned off.'),
    '#options' => array(
      0 => t('Disabled'),
      'single' => t('Single field'),
      'double' => t('Double fields'),
    ),
    '#default_value' => (isset($values['name_field_setting']) ? $values['name_field_setting'] : $default_name_setting),
  );

  $form['lists'] = array(
    '#type' => 'fieldset',
    '#title' => t('Newsletter lists'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  // Newsletter lists settings.
  $options = array();
  $mailinglists = peytz_mail_get_mailing_lists();
  if (!empty($mailinglists)) {
    foreach ($mailinglists as $list) {
      $options[$list['id']] = $list['title'];
    }
    $selected_options = array();
    if (isset($values['newsletter_lists'])) {
      $conf_newsletter_lists = is_array($values['newsletter_lists']) ? $values['newsletter_lists'] : unserialize($values['newsletter_lists']);
      if (!empty($conf_newsletter_lists)) {
        foreach ($conf_newsletter_lists as $conf_list) {
          $selected_options[] = $conf_list['id'];
        }
      }
    }
    else {
      $selected_options = variable_get('peytz_mail_default_newsletter_lists', array());
    }

    $form['lists']['newsletter_lists'] = array(
      '#type' => 'select',
      '#title' => t('Newsletter lists'),
      '#multiple' => TRUE,
      '#description' => t('Select which newsletter lists this sign up box is connected to. If multiple lists are selected they will be presented as checkboxes in the sign up box.'),
      '#options' => $options,
      '#default_value' => $selected_options,
    );

    $default_hide_lists_setting = variable_get('peytz_mail_default_hide_newsletter_lists', -1);
    $form['lists']['hide_newsletter_lists'] = array(
      '#type' => 'checkbox',
      '#title' => t('Hide newsletter lists from users.'),
      '#default_value' => (!empty($values['hide_newsletter_lists']) ? $values['hide_newsletter_lists'] : $default_hide_lists_setting),
      '#description' => t('Check this if the users are not allowed to select which lists to join.'),
    );
  }
  else {
    $form['lists']['no_lists'] = array(
      '#markup' => t('No public newsletter lists found. Please check your Peytz Mail configuration.'),
    );
  }

  $form['signup_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Signup settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $default_skip_confirm_setting = variable_get('peytz_mail_default_skip_confirm', -1);
  $form['signup_settings']['skip_confirm'] = array(
    '#type' => 'checkbox',
    '#title' => t('Skip confirmation mails.'),
    '#default_value' => (!empty($values['skip_confirm']) ? $values['skip_confirm'] : $default_skip_confirm_setting),
  );

  $default_skip_welcome_setting = variable_get('peytz_mail_default_skip_welcome', -1);
  $form['signup_settings']['skip_welcome'] = array(
    '#type' => 'checkbox',
    '#title' => t('Skip welcome mails.'),
    '#default_value' => (!empty($values['skip_welcome']) ? $values['skip_welcome'] : $default_skip_welcome_setting),
  );
}
